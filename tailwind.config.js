module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    spacing: {
      '1': '1.5rem',
      '2': '2rem',
      '3': '5rem',
      '4': '10rem',
      '5': '20rem'
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
