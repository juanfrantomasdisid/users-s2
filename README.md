# Test S2

Simple SPA to display users in a table.

## Technologies
The following technologies have been used for its development
- Vue3
- Vite
- Tailwind CSS

## Usage

```bash
npm run dev
```
or if you want to display it on your local network
```bash
npm run dev:host
```

## Demo
[Test S2 - Netlify](fervent-boyd-9fa0de.netlify.app)
## Author
Juanfran Tomas Planells
